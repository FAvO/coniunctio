<?php
/*
Coniunctio
Copyright (C) 2020 Felix v. Oertzen
coniunctio@von-oertzen-berlin.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function generateRandomToken() {
  $token = openssl_random_pseudo_bytes(8);
  $token = bin2hex($token);
  return $token;
}

$db = new PDO('sqlite:shortlinkdatabase.db');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db-> exec("CREATE TABLE IF NOT EXISTS shortlinks(
          task INTEGER PRIMARY KEY NOT NULL,
          shortlink VARCHAR(128) NOT NULL,
          destination VARCHAR(256) NOT NULL,
          creation DATE NOT NULL,
          expiration DATE,
          ip VARCHAR(128) NOT NULL,
          host VARCHAR(256) NOT NULL,
          useragent VARCHAR(256) NOT NULL,
          remaining INTEGER DEFAULT -1)");


if (!($db->query("SELECT * FROM shortlinks WHERE task = 0")->fetch())) {
  // Coniunctio is not installed
  rename("htaccess", ".htaccess");
  $sec = generateRandomToken();
  $has = password_hash($sec,PASSWORD_DEFAULT);
  require_once("header.include");
  require_once("body.include");

  $ie = $db->prepare("INSERT INTO shortlinks (task, shortlink, destination, creation, expiration, ip, host, useragent) VALUES (:tsk,:shrtlnk, :dstntn, :crtn, :exprtn, :ip, :host, :useragent)");
  $a = 0;
  $ie->bindParam(':tsk', $a, PDO::PARAM_INT);
  $ie->bindParam(':shrtlnk', $has);
  $b = "";
  $ie->bindParam(':dstntn', $b);
  $ie->bindParam(':crtn', date("Y-m-d"));
  $c = "9999-12-31";
  $ie->bindParam(':exprtn', $c);
  $ie->bindParam(':ip', $_SERVER['REMOTE_ADDR']);
  $d = ($_SERVER['REMOTE_HOST'] ? $_SERVER['REMOTE_HOST'] : "no hostname found");
  $ie->bindParam(':host', $d);
  $ie->bindParam(':useragent', $_SERVER['HTTP_USER_AGENT']);
  $ie->execute();
  secretcode($sec);
  die();
}

if ($_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/index.php") {
header("location: /creator.php");
die();
} else {
  $sl = substr($_SERVER['REQUEST_URI'],strpos($_SERVER["REQUEST_URI"],"/")+1);

  $statement = $db->prepare("SELECT * FROM shortlinks WHERE shortlink = ?");
  $statement->bindParam(1, $sl);
  $statement->execute(); $row = $statement->fetch();
  if ($row) {
    if ((int)($row['remaining']) > 0 || (int)($row['remaining']) == -1) {
      header("Location: " . $row['destination']);
      if ($row['remaining'] > 0) {
         $db->query("UPDATE shortlinks SET remaining = "
             . (string)( ((int)($row['remaining'])) - 1)
             . " WHERE task = " . $row['task']);
      }
    } else {
      require_once("header.include");
      require_once("body.include");
      usedtomuch();
    }
  } else {
    require_once("header.include");
    require_once("body.include");
    linkdoesntexist();
  }
}
?>
